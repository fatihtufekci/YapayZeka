-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: yapayzeka
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `derbiverisi`
--

DROP TABLE IF EXISTS `derbiverisi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `derbiverisi` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Sezon` varchar(15) NOT NULL,
  `MacDurumu` varchar(20) DEFAULT NULL,
  `Mekan` varchar(20) NOT NULL,
  `BJK` int(11) DEFAULT NULL,
  `GS` int(11) DEFAULT NULL,
  `BJKPuan` int(11) NOT NULL,
  `MacHaftasi` int(11) NOT NULL,
  `GSPuan` int(11) NOT NULL,
  `BJKYedigi` int(11) NOT NULL,
  `BJKAttigi` int(11) NOT NULL,
  `GSYedigi` int(11) NOT NULL,
  `GSAttigi` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `derbiverisi`
--

LOCK TABLES `derbiverisi` WRITE;
/*!40000 ALTER TABLE `derbiverisi` DISABLE KEYS */;
INSERT INTO `derbiverisi` VALUES (55,'1990-1991','Beraberlik','Ic',1,1,9,12,10,2,6,5,5),(56,'1990-1991','Galibiyet','Dis',3,2,27,27,30,11,24,11,31),(57,'1991-1992','Galibiyet','Ic',4,3,33,29,29,8,26,11,23),(58,'1991-1992','Galibiyet','Dis',1,0,16,14,16,7,13,6,10),(59,'1992-1993','Maglubiyet','Ic',1,3,19,14,17,3,19,2,13),(60,'1992-1993','Beraberlik','Dis',1,1,28,29,32,13,30,11,39),(61,'1993-1994','Maglubiyet','Ic',0,1,10,10,7,2,9,6,7),(62,'1993-1994','Beraberlik','Dis',1,1,18,25,28,12,16,10,30),(63,'1994-1995','Maglubiyet','Ic',2,3,30,24,20,6,35,12,24),(64,'1994-1995','Maglubiyet','Dis',1,3,9,7,9,1,7,3,8),(65,'1995-1996','Maglubiyet','Ic',1,2,37,33,28,24,47,23,29),(66,'1995-1996','Galibiyet','Dis',3,1,11,16,18,10,12,5,19),(67,'1996-1997','Beraberlik','Ic',1,1,38,30,35,5,40,12,39),(68,'1996-1997','Beraberlik','Dis',2,2,11,13,15,5,13,6,16),(69,'1997-1998','Galibiyet','Ic',2,1,9,7,2,2,7,4,2),(70,'1997-1998','Maglubiyet','Dis',2,3,13,24,29,16,22,9,29),(71,'1998-1999','Beraberlik','Ic',1,1,30,31,37,15,25,10,38),(72,'1998-1999','Maglubiyet','Dis',0,2,19,14,15,2,14,10,20),(73,'1999-2000','Beraberlik','Ic',1,1,35,29,34,6,31,10,24),(74,'1999-2000','Maglubiyet','Dis',0,1,8,12,12,5,11,3,14),(75,'2000-2001','Galibiyet','Ic',3,1,9,9,12,5,10,2,11),(76,'2000-2001','Maglubiyet','Dis',0,2,18,26,23,23,22,11,27),(77,'2001-2002','Beraberlik','Ic',2,2,6,9,10,5,9,5,12),(78,'2001-2002','Maglubiyet','Dis',0,1,27,26,39,14,34,8,38),(79,'2002-2003','Galibiyet','Ic',1,0,42,16,36,7,33,17,25),(80,'2002-2003','Galibiyet','Dis',1,0,14,33,21,6,10,3,16),(81,'2003-2004','Beraberlik','Ic',0,0,15,11,10,1,13,4,7),(82,'2003-2004','Galibiyet','Dis',2,1,25,28,27,16,24,19,25),(83,'2004-2005','Beraberlik','Ic',0,0,0,6,3,4,3,2,2),(84,'2004-2005','Maglubiyet','Dis',0,1,21,23,28,13,20,7,27),(85,'2005-2006','Maglubiyet','Ic',1,2,22,16,34,18,23,17,28),(86,'2005-2006','Maglubiyet','Dis',2,3,11,33,16,6,8,6,16),(87,'2006-2007','Galibiyet','Ic',2,1,25,23,10,10,20,14,10),(88,'2006-2007','Maglubiyet','Dis',0,1,3,6,3,1,2,2,6),(89,'2007-2008','Galibiyet','Ic',1,0,24,24,25,11,20,9,19),(90,'2007-2008','Maglubiyet','Dis',1,2,7,7,9,1,3,0,11),(91,'2008-2009','Galibiyet','Ic',2,1,36,33,24,11,27,19,21),(92,'2008-2009','Maglubiyet','Dis',2,4,9,16,19,7,8,5,20),(93,'2009-2010','Beraberlik','Ic',1,1,20,22,19,6,15,14,17),(94,'2009-2010','Maglubiyet','Dis',0,3,2,5,6,1,1,2,8),(95,'2010-2011','Galibiyet','Ic',2,0,23,31,17,23,30,21,16),(96,'2010-2011','Galibiyet','Dis',2,1,10,14,9,4,8,10,8),(97,'2011-2012','Beraberlik','Ic',0,0,10,11,8,6,9,3,6),(98,'2011-2012','Maglubiyet','Dis',2,3,19,28,32,14,15,11,30),(99,'2012-2013','Beraberlik','Ic',3,3,0,2,0,0,0,0,0),(100,'2012-2013','Maglubiyet','Dis',1,2,14,19,17,13,17,9,16),(101,'2013-2014','Maglubiyet','Ic',0,3,6,5,2,0,4,1,1),(102,'2013-2014','Maglubiyet','Dis',0,1,21,22,26,13,23,7,23),(112,'2014-2015','Beraberlik','Ic',1,1,14,16,16,7,13,9,13),(113,'2007-2008','Beraberlik','Ic',1,1,12,12,12,12,12,12,12),(114,'2014-2015','Beraberlik','Ic',0,0,37,33,28,24,47,23,29);
/*!40000 ALTER TABLE `derbiverisi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'yapayzeka'
--

--
-- Dumping routines for database 'yapayzeka'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-10  2:14:11
