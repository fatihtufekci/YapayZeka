package dolphy.yapayzeka.fatih;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

import dolphy.yapayzeka.classification.JDBCUtil;

public class Classification {
	
	private static Connection conn;
	private static PreparedStatement pstmt;
	private static ResultSet rs;

	private static int countGalibiyet = 0;
	private static int countBeraberlik = 0;
	private static int countMaglubiyet = 0;
	private static int rowCount = 0;

	// KNN algoritmasında 3 sınıfımız old icin K=5 dedik.
	private static final int K = 5;

	private static final String GALIBIYET = "Galibiyet";
	private static final String BERABERLIK = "Beraberlik";
	private static final String MAGLUBIYET = "Maglubiyet";

	private static final String[] COLUMNS = { "BJKPuan", "GSPuan", "BJKYedigi", "BJKAttigi", "GSYedigi", "GSAttigi" };

	private static final String BJK = "BJK";
	private static final String GS = "GS";

	private static int[][] galibiyetArray = null;
	private static int[][] beraberlikArray = null;
	private static int[][] maglubiyetArray = null;

	private static int[] testArray = null;

	private static int[] galibiyetAvarage;
	private static int[] beraberlikAvarage;
	private static int[] maglubiyetAvarage;

	private static String testMekan;

	private static double galibiyetEuclid = 0.0;
	private static double beraberlikEuclid = 0.0;
	private static double maglubiyetEuclid = 0.0;

	private static int guessRowCount = 0;

	private static String query = "select * from derbiverisi where MacDurumu = ?";
	private static String query1 = "select * from derbiverisi where Mekan = ?";
	private static String query2 = "select * from derbiverisi where MacDurumu = ? and Mekan = ?";
	private static String query3 = "update derbiverisi set BJK = ?, GS = ? where MacDurumu = ?";

	public static void main(String[] args) {
		conn = JDBCUtil.getConnection();

		classficationAlgorithms();
	}

	public static void classficationAlgorithms() {
		System.out.println("\nQuery: " + query);
		System.out.println("--------------------------------------------------------------------");

		testArray = new int[COLUMNS.length];
		galibiyetAvarage = new int[COLUMNS.length];
		beraberlikAvarage = new int[COLUMNS.length];
		maglubiyetAvarage = new int[COLUMNS.length];

		try {
			pstmt = conn.prepareStatement(query);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		try {

			rowCount = takeTheNumberOfRowsInTheClasses(GALIBIYET);
			galibiyetArray = new int[COLUMNS.length][rowCount];

			rowCount = takeTheNumberOfRowsInTheClasses(BERABERLIK);
			beraberlikArray = new int[COLUMNS.length][rowCount];

			rowCount = takeTheNumberOfRowsInTheClasses(MAGLUBIYET);
			maglubiyetArray = new int[COLUMNS.length][rowCount];

			takeTestData();

			compareTestDataWithOurClasses(GALIBIYET);
			compareTestDataWithOurClasses(BERABERLIK);
			compareTestDataWithOurClasses(MAGLUBIYET);

			sort(galibiyetArray);
			sort(beraberlikArray);
			sort(maglubiyetArray);

			// S�n�flandırma olarak knn algoritmasını kullanıyoruz.
			knn();

			System.out.println("Galibiyet: " + countGalibiyet);
			System.out.println("Beraberlik: " + countBeraberlik);
			System.out.println("Maglubiyet: " + countMaglubiyet);

			int[] c = { countGalibiyet, countBeraberlik, countMaglubiyet };
			Arrays.sort(c);
			System.out.println();
			System.out.println("knn algoritmamız;");
			if (c[2] == countGalibiyet) {
				System.out.println("Test verimiz GALIBIYET'tir");
			} else if (c[2] == countBeraberlik) {
				System.out.println("Test verimiz BERABERLIK'dir.");
			} else if (c[2] == countMaglubiyet) {
				System.out.println("Test verimiz MAGLUBIYET'tir");
			}

			double enkucuk = euclidMethod();

			System.out.println();

			String macDurumu = null;
			if (enkucuk == galibiyetEuclid) {
				macDurumu = GALIBIYET;
			} else if (enkucuk == beraberlikEuclid) {
				macDurumu = BERABERLIK;
			} else if (enkucuk == maglubiyetEuclid) {
				macDurumu = MAGLUBIYET;
			}

			guessRowCount = takeTheNumberOfRowsInTheClasses(macDurumu);
			System.out.println("Tahmin algoritmam�z; ");
			System.out.println("BJK: " + guessAlgorithmTeams(BJK, macDurumu) + " - "
					+ guessAlgorithmTeams(GS, macDurumu) + " :GS");
			int b = guessAlgorithmTeams(BJK, macDurumu);
			int g = guessAlgorithmTeams(GS, macDurumu);

			// System.out.println("tüm veri satırı: " + dataSetRowCount());

			updateTest(b, g);

			pstmt.close();
			conn.close();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

	}

	private static double euclidMethod() throws SQLException {
		galibiyetAvarage = findTheAvaregeValueOfTheClasses(GALIBIYET);
		beraberlikAvarage = findTheAvaregeValueOfTheClasses(BERABERLIK);
		maglubiyetAvarage = findTheAvaregeValueOfTheClasses(MAGLUBIYET);

		galibiyetEuclid = euclid(galibiyetAvarage);
		beraberlikEuclid = euclid(beraberlikAvarage);
		maglubiyetEuclid = euclid(maglubiyetAvarage);

		double enkucuk = 0.0;
		enkucuk = (int) galibiyetEuclid;
		if (beraberlikEuclid < enkucuk)
			enkucuk = beraberlikEuclid;
		if (maglubiyetEuclid < enkucuk)
			enkucuk = maglubiyetEuclid;

		System.out.println();
		System.out.println("Euclid; ");
		if (galibiyetEuclid < beraberlikEuclid && galibiyetEuclid <= maglubiyetEuclid) {
			System.out.println("Test verimiz GALIBIYET't�r");
		} else if (beraberlikEuclid <= galibiyetEuclid && beraberlikEuclid <= maglubiyetEuclid) {
			System.out.println("Test verimiz BERABERLIK'dir.");
		} else if (maglubiyetEuclid < galibiyetEuclid && maglubiyetEuclid < beraberlikEuclid) {
			System.out.println("Test verimiz MAGLUBIYET'tir");
		}
		return enkucuk;
	}

	private static int takeTheNumberOfRowsInTheClasses(final String classes) throws SQLException {
		pstmt.setString(1, classes);
		int row = 0;
		pstmt.execute();
		rs = pstmt.getResultSet();
		while (rs.next()) {
			row = rs.getRow();
		}
		return row;
	}

	private static void takeTestData() throws SQLException {
		int i = 0;
		pstmt.setString(1, "Test");

		while (i < COLUMNS.length) {
			pstmt.execute();
			rs = pstmt.getResultSet();
			while (rs.next()) {
				testMekan = rs.getString("Mekan");
				testArray[i] = rs.getInt(COLUMNS[i]);
			}

			i++;
		}
	}

	private static void compareTestDataWithOurClasses(final String classes) throws SQLException {
		int i = 0;
		int diff = 0;
		int countG = 0;
		int countB = 0;
		int countM = 0;
		int currentRow = 0;
		int mutlak = 0;
		pstmt = conn.prepareStatement(query2);
		pstmt.setString(1, classes);
		pstmt.setString(2, testMekan);

		while (i < COLUMNS.length) {
			pstmt.execute();
			rs = pstmt.getResultSet();
			countB = 0;
			countG = 0;
			countM = 0;
			while (rs.next()) {
				currentRow = rs.getInt(COLUMNS[i]);
				diff = currentRow - testArray[i];
				mutlak = Math.abs(diff);

				if (classes == GALIBIYET) {
					galibiyetArray[i][countG] = mutlak;
					countG++;
				} else if (classes == BERABERLIK) {
					beraberlikArray[i][countB] = mutlak;
					countB++;
				} else if (classes == MAGLUBIYET) {
					maglubiyetArray[i][countM] = mutlak;
					countM++;
				}
			}

			i++;
		}
	}

	private static void knn() {
		int i = 0;
		int j = 0;
		while (i < COLUMNS.length) {
			j = 0;
			while (j < K) {
				if (galibiyetArray[i][j] < beraberlikArray[i][j] && galibiyetArray[i][j] <= maglubiyetArray[i][j]) {
					countGalibiyet++;
				} else if (beraberlikArray[i][j] <= galibiyetArray[i][j]
						&& beraberlikArray[i][j] <= maglubiyetArray[i][j]) {
					countBeraberlik++;
				} else if (maglubiyetArray[i][j] < galibiyetArray[i][j]
						&& maglubiyetArray[i][j] < beraberlikArray[i][j]) {
					countMaglubiyet++;
				}
				j++;
			}
			i++;
		}
	}

	private static void sort(int dizi[][]) {
		for (int i = 0; i < dizi.length; i++) {
			Arrays.sort(dizi[i]);
		}
	}

	// private static void display(int dizi[][]) {
	// for (int row = 0; row < dizi.length; row++) {
	// for (int column = 0; column < dizi[row].length; column++) {
	// System.out.print(dizi[row][column] + "\t");
	// }
	// System.out.println();
	// }
	// }

	private static int[] findTheAvaregeValueOfTheClasses(final String classes) throws SQLException {
		pstmt.setString(1, classes);
		int i = 0;
		int t = 0;
		int toplam = 0;
		int avg = 0;
		int[] dizi = new int[COLUMNS.length];
		while (i < COLUMNS.length) {
			pstmt.execute();
			rs = pstmt.getResultSet();
			toplam = 0;
			t = 0;
			while (rs.next()) {
				t = rs.getInt(COLUMNS[i]);
				toplam = toplam + t;
			}
			if(takeTheNumberOfRowsInTheClasses(classes)!=0)
				avg = toplam / takeTheNumberOfRowsInTheClasses(classes);
			dizi[i] = avg;
			i++;
		}
		return dizi;
	}

	private static double euclid(int[] dizi) {
		double t = 0;
		for (int i = 0; i < COLUMNS.length; i++) {
			t = t + Math.pow(dizi[i] - testArray[i], 2);
		}
		return Math.sqrt(t);
	}

	private static int dataSetRowCount() throws SQLException {
		int rowData = 0;
		int row = 0;
		pstmt = conn.prepareStatement(query1);
		pstmt.setString(1, testMekan);
		pstmt.execute();
		rs = pstmt.executeQuery();
		while (rs.next()) {
			rowData = rs.getRow();
		}
		row = rowData - 1; // test verisinin satırını cıkardık.
		return row;
	}

	private static int guessAlgorithmTeams(final String column, final String macDurumu) throws SQLException {
		double t = 0.0;
		double avg = 0;
		pstmt = conn.prepareStatement(query2);

		pstmt.setString(1, macDurumu);
		pstmt.setString(2, testMekan);
		pstmt.execute();
		rs = pstmt.getResultSet();
		while (rs.next()) {
			t = t + rs.getInt(column);
		}
		avg = Math.ceil(t / guessRowCount);
		return (int) avg;
	}

	private static void updateTest(int updateValueBJK, int updateValueGS) throws SQLException {
		pstmt = conn.prepareStatement(query3);
		pstmt.setInt(1, updateValueBJK);
		pstmt.setInt(2, updateValueGS);
		pstmt.setString(3, "Test");
		// projeyi vermeden önce MacDurumu'nuda set edip sonuc neyse onu
		// yapabilirisiniz.
		pstmt.executeUpdate();
	}
	
}
